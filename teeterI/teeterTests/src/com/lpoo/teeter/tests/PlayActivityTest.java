package com.lpoo.teeter.tests;

import android.hardware.SensorEvent;
import android.hardware.SensorManager;
import android.test.ActivityInstrumentationTestCase2;
import com.lpoo.teeter.controllers.PlayActivity;

/**
 * This is a simple framework for a test of an Application.  See
 *{@link android.test.ApplicationTestCase ApplicationTestCase} for more information on
 * how to write and extend Application tests.
 * <p/>
 * To run this test, you can type:
 * adb shell am instrument -w \
 * -e class com.lpoo.teeter.controllers.MenuActivityTest \
 * com.lpoo.teeter.tests/android.test.InstrumentationTestRunner
 */
public class PlayActivityTest extends ActivityInstrumentationTestCase2<PlayActivity> {

    private PlayActivity playInstance;

    /**
     * Instantiates a new Play activity test.
     */
    public PlayActivityTest() {
        super("com.lpoo.teeter", PlayActivity.class);
    }

    @Override
    protected void setUp() throws Exception {
        super.setUp();
        playInstance = getActivity();
    }

    /**
     * Test wall collision.
     */
    public void testWallCollision() {
        playInstance.getGame().resetBall();

        for(int i = 0; i < 999999; i++)
            playInstance.sensorChanged(99999, -100);

        int x = (int)playInstance.getGame().getWorld().getBody(0).getTransform().getTranslationX()*10;
        int y = (int)playInstance.getGame().getWorld().getBody(0).getTransform().getTranslationY()*10;

        if(x > (960/2)-10 || x < 10)
            x = -1;

        if(y < 10 || y > 20)
            y = -1;

        assertNotSame(x, -1);
        assertNotSame(y, -1);
    }

    /**
     * Test lost.
     */
    public void testLost() {
        playInstance.getGame().resetBall();

        for(int i = 0; i < 999999; i++)
            playInstance.sensorChanged(0, 99999);

        int x = (int)playInstance.getGame().getWorld().getBody(0).getTransform().getTranslationX()*10;
        int y = (int)playInstance.getGame().getWorld().getBody(0).getTransform().getTranslationY()*10;

        assertTrue(playInstance.getGame().isLose());
        assertTrue(playInstance.getGame().isRest());
        assertFalse(playInstance.getGame().isWon());
    }

    /**
     * Test won.
     */
    public void testWon() {
        playInstance.getGame().resetBall();

        for(int i = 0; i < 999999; i++)
            playInstance.sensorChanged(99999, 0);

        for(int i = 0; i < 999999; i++)
            playInstance.sensorChanged(0, 99999);

        for(int i = 0; i < 999999; i++)
            playInstance.sensorChanged(99999, 0);

        int x = (int)playInstance.getGame().getWorld().getBody(0).getTransform().getTranslationX()*10;
        int y = (int)playInstance.getGame().getWorld().getBody(0).getTransform().getTranslationY()*10;

        assertTrue(playInstance.getGame().isRest());
        assertFalse(playInstance.getGame().isLose());
        assertTrue(playInstance.getGame().isWon());
    }








}
