package com.lpoo.teeter.models;

import java.io.*;
import java.util.ArrayList;


/**
 * The type Files.
 */
public class Files {

    private static Files singletonObject;
    /**
     * The Filename.
     */
    public String filename = "highscore.txt";
    /**
     * The Matrix.
     */
    public int matrix[][];
    /**
     * The Walls.
     */
    public ArrayList<Coord> walls;
    /**
     * The Ball.
     */
    public Coord ball;
    /**
     * The Final hole.
     */
    public Coord finalHole = null;
    /**
     * The Holes.
     */
    public ArrayList<Coord> holes;

    private Files() {
        //	 Optional Code
    }

    /**
     * Get files.
     *
     * @return the files
     */
    public static synchronized Files get() {
        if (singletonObject == null) {
            singletonObject = new Files();
        }
        return singletonObject;
    }

    public Object clone() throws CloneNotSupportedException {
        throw new CloneNotSupportedException();
    }

    /**
     * Reader input.
     *
     * @param is the is
     * @throws IOException the iO exception
     */
    public void readerInput(InputStream is) throws IOException {
        matrix = new int[18][32];
        BufferedReader br = new BufferedReader(new InputStreamReader(is, "UTF-8"));
        try {
            String numberList = br.readLine();
            int j = 0;

            while (numberList != null) {

                for (int i = 0; i < 32; i++) {
                    matrix[j][i] = Integer.parseInt(String.valueOf(numberList.charAt(i)));
                }
                j++;
                numberList = br.readLine();
            }
        } finally {
            br.close();
        }
    }

    /**
     * Transform void.
     */
    public void transform() {
        walls = new ArrayList<Coord>();
        holes = new ArrayList<Coord>();

        // Default final hole
        finalHole = new Coord(15, 15, 15, 15);

        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[i].length; j++) {
                // y - j
                if (matrix[i][j] == 1)
                    walls.add(new Coord(j * 30, (j + 1) * 30, i * 30, (i + 1) * 30));
                else if (matrix[i][j] == 2)
                    holes.add(new Coord(j * 30, (j + 1) * 30, i * 30, (i + 1) * 30));
                else if (matrix[i][j] == 3)
                    ball = new Coord(j * 30 + 15, j * 30 + 15, i * 30 + 15, i * 30 + 15);
                else if (matrix[i][j] == 4)
                    finalHole = new Coord(j * 30 + 15, j * 30 + 15, i * 30 + 15, i * 30 + 15);

            }
        }

        boolean encontrou = true;

        while (encontrou) {
            encontrou = false;
            for (int i = 0; i < walls.size(); i++) {
                for (int j = 0; j < walls.size(); j++) {
                    if (walls.get(j).getXi() == walls.get(i).getXf() && i != j && walls.get(i).getYi() == walls.get(j).getYi() && walls.get(i).getYf() == walls.get(j).getYf()) {
                        walls.get(i).setXf(walls.get(i).getXf() + 30);
                        walls.remove(j);
                        j--;
                        encontrou = true;
                    }

                    if (walls.get(i).getYf() == walls.get(j).getYi() && i != j && walls.get(i).getXi() == walls.get(j).getXi() && walls.get(i).getXf() == walls.get(j).getXf()) {
                        walls.get(i).setYf(walls.get(i).getYf() + 30);
                        walls.remove(j);
                        j--;
                        encontrou = true;
                    }
                }
            }
        }

        walls.add(new Coord(-10, 0, 0, 540));
        walls.add(new Coord(960, 970, 0, 540));
        walls.add(new Coord(0, 960, -10, 0));
        walls.add(new Coord(0, 960, 540, 550));
    }

    /**
     * Method to write ascii text characters to file on SD card. Note that you must add a
     * WRITE_EXTERNAL_STORAGE permission to the manifest file or this method will throw
     * a FileNotFound Exception because you won't have write permission.  @param levelNumber the level number
     */


    public void writeToSDFile(String levelNumber) {

        File root = android.os.Environment.getExternalStorageDirectory();

        File dir = new File(root.getAbsolutePath() + "/Android/data/com.lpoo.teeter/");
        dir.mkdirs();
        File file = new File(dir, "highscore.txt");

        try {
            FileOutputStream f = new FileOutputStream(file);
            PrintWriter pw = new PrintWriter(f);
            pw.println(levelNumber);
            pw.flush();
            pw.close();
            f.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    /**
     * Read file.
     *
     * @return the string
     */
    public String readFile() {
        File root = android.os.Environment.getExternalStorageDirectory();

        File dir = new File(root.getAbsolutePath() + "/Android/data/com.lpoo.teeter/");

        File file = new File(dir, "highscore.txt");
        String pw = "1";
        try {
            FileInputStream f = new FileInputStream(file);

            InputStreamReader isr = new InputStreamReader(f);
            BufferedReader br = new BufferedReader(isr, 8192);
            pw = br.readLine();


            f.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return pw;

    }


    /** Method to read in a text file placed in the res/raw directory of the application. The
     method reads in all lines of the file sequentially. */


}
