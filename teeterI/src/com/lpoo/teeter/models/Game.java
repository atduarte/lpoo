package com.lpoo.teeter.models;

import org.dyn4j.dynamics.Body;
import org.dyn4j.dynamics.BodyFixture;
import org.dyn4j.dynamics.Settings;
import org.dyn4j.dynamics.World;
import org.dyn4j.geometry.Circle;
import org.dyn4j.geometry.Mass;
import org.dyn4j.geometry.Rectangle;
import org.dyn4j.geometry.Vector2;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;


/**
 * The type Game.
 */
public class Game {

    World world = null;
    Settings worldSettings;

    boolean rest = false;
    boolean lose = false;
    boolean won = false;

    ArrayList<Coord> walls;
    ArrayList<Circle> holes;

    Circle finalHole;

    Coord initialBall = null;

    /**
     * Instantiates a new Game.
     *
     * @param stream the stream
     * @throws IOException the iO exception
     */
    public Game(InputStream stream) throws IOException
	{

        Files.get().readerInput(stream);
        Files.get().transform();

        // World
        world = new World();
        world.setGravity(new Vector2(0,0));

        // World Settings
        worldSettings = new Settings();
        worldSettings.setContinuousDetectionMode(Settings.ContinuousDetectionMode.NONE);
        world.setSettings(worldSettings);

        // Ball

        initialBall = Files.get().ball;

        Circle cirShape = new Circle(1.5);
        BodyFixture f = new BodyFixture(cirShape);
        f.setRestitution(0.35);
        f.setFriction(0.2);

        Body circle = new Body();
        circle.addFixture(f);
        circle.setMass();
        circle.translate(initialBall.getXi()/10, initialBall.getYi()/10);

        this.world.addBody(circle);

        // Walls

        walls = new ArrayList<Coord>(Files.get().walls);

        for(int i=0;i<walls.size();i++)
        {
            double  w = Math.abs(walls.get(i).getXf()-walls.get(i).getXi())/10,
                    h = Math.abs(walls.get(i).getYf()-walls.get(i).getYi())/10;

            Rectangle floorRect = new Rectangle(w,h);
            Body floor = new Body();
            floor.addFixture(floorRect);
            floor.setMass(Mass.Type.INFINITE);

            double  x = walls.get(i).getXi()/10,
                    y = walls.get(i).getYi()/10;
            floor.translate((w/2) + x, (h/2) + y);

            this.world.addBody(floor);
        }

        // Holes
        holes = new ArrayList<Circle>();
        ArrayList<Coord> holestemp = new ArrayList<Coord>(Files.get().holes) ;
        for(int i=0;i<holestemp.size();i++)
        {
            Circle hole = new Circle(30);
            hole.translate(holestemp.get(i).getXi()+15,holestemp.get(i).getYi()+15);
            holes.add(hole);
        }

        // Final Hole
        finalHole = new Circle(30);
        finalHole.translate(Files.get().finalHole.getXi(), Files.get().finalHole.getYi());
	}

    /**
     * Is rest.
     *
     * @return the boolean
     */
    public boolean isRest() {
        return rest;
    }

    /**
     * Sets rest.
     *
     * @param rest the rest
     */
    public void setRest(boolean rest) {
        this.rest = rest;
    }

    /**
     * Gets world.
     *
     * @return the world
     */
    public World getWorld() {
        return world;
    }

    /**
     * Gets holes.
     *
     * @return the holes
     */
    public ArrayList<Circle> getHoles() {
        return holes;
    }

    /**
     * Gets walls.
     *
     * @return the walls
     */
    public ArrayList<Coord> getWalls() {
        return walls;
    }

    /**
     * Gets final hole.
     *
     * @return the final hole
     */
    public Circle getFinalHole() {
        return finalHole;
    }

    /**
     * Is won.
     *
     * @return the boolean
     */
    public boolean isWon() {
        return won;
    }

    /**
     * Sets won.
     *
     * @param won the won
     */
    public void setWon(boolean won) {
        this.won = won;
    }

    /**
     * Is lose.
     *
     * @return the boolean
     */
    public boolean isLose() {
        return lose;
    }

    /**
     * Sets lose.
     *
     * @param lose the lose
     */
    public void setLose(boolean lose) {
        this.lose = lose;
    }

    /**
     * Reset ball.
     */
    public  void resetBall() {
        world.getBody(0).setVelocity(new Vector2(0,0));
        world.getBody(0).getTransform().setTranslationX(initialBall.getXi()/10);
        world.getBody(0).getTransform().setTranslationY(initialBall.getYi()/10);
    }
}
