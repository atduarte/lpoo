package com.lpoo.teeter.controllers;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import com.lpoo.teeter.R;
import com.lpoo.teeter.core.FullscreenActivity;

import java.util.ArrayList;

/**
 * The type Levels activity.
 */
public class LevelsActivity extends FullscreenActivity implements View.OnClickListener {


    ArrayList<Button> levelButtons = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Set Content
        setContentView(R.layout.activity_levels);

        initializeLevelButtons();

    }

    @Override
    public void onClick(View v) {

        for(int i = 0; i < levelButtons.size(); i++){
            if(v.getId() == levelButtons.get(i).getId()) {
                Intent intent = new Intent(LevelsActivity.this, PlayActivity.class);
                intent.putExtra("LEVEL_ID", String.valueOf(i+1));
                startActivity(intent);
                finish();
            }
        }
    }

    private void initializeLevelButtons() {
        levelButtons = new ArrayList<Button>();
        levelButtons.add( (Button) findViewById(R.id.level1) );
        levelButtons.add( (Button) findViewById(R.id.level2) );
        levelButtons.add( (Button) findViewById(R.id.level3) );
        levelButtons.add( (Button) findViewById(R.id.level4) );
        levelButtons.add( (Button) findViewById(R.id.level5) );
        levelButtons.add( (Button) findViewById(R.id.level6) );
        levelButtons.add( (Button) findViewById(R.id.level7) );
        levelButtons.add( (Button) findViewById(R.id.level8) );
        levelButtons.add( (Button) findViewById(R.id.level9) );

        for(int i = 0; i < levelButtons.size(); i++) {
            if(i <  unlockedLevel) {
                levelButtons.get(i).setOnClickListener(this);
            } else
                levelButtons.get(i).setEnabled(false);
        }
    }

}