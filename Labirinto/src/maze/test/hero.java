package maze.test;

import static org.junit.Assert.*;


import maze.logic.Game;
import maze.logic.Maze;

import org.junit.Test;

public class hero {


	
	char [][] campo={{'X','X','X','X','X'},{'X',' ',' ',' ','X'},{'X',' ','H',' ','X'},{'X',' ',' ',' ','X'},{'X','X','X','X','X'}};
	char [][] campo2={{'X','X','X','X','X'},{'X','X','X','X','X'},{'X','X','H','X','X'},{'X','X','X','X','X'},{'X','X','X','X','X'}};
	char [][] campo3={{'X','X','X','X','X'},{'X','X','X','X','X'},{'X','E','H','X','X'},{'X','X','X','X','X'},{'X','X','X','X','X'}};
	char [][] campo4={{'X','X','X','X','X'},{'X','X','X','X','X'},{'X','X','H','E','X'},{'X','X','X','X','X'},{'X','X','X','X','X'}};
	char [][] campo5={{'X','X','X','X','X'},{'X','X','X','X','X'},{'X','X','H','X','X'},{'X','X','E','X','X'},{'X','X','X','X','X'}};
	char [][] campo6={{'X','X','X','X','X'},{'X','X','E','X','X'},{'X','X','H','X','X'},{'X','X','X','X','X'},{'X','X','X','X','X'}};
	

	char [][] campo7={{'X','S','X'},{'S','H','S'},{'X','S','X'}};
	char [][] campo8={{'X','X','X','X','X'},{'X',' ',' ',' ','X'},{'X','E','H',' ','X'},{'X',' ',' ',' ','X'},{'X','X','X','X','X'}};
	
	char [][] campo9={	{'X','X','X','X','X'},
						{'X',' ','E','H','X'},
						{'X',' ','D',' ','X'},
						{'X',' ',' ',' ','X'},
						{'X','X','X','S','X'}};
	
	char [][] campo10={	{'X','X','X','X','X'},
						{'X',' ',' ',' ','X'},
						{'X','D',' ','E','X'},
						{'X',' ',' ','H','X'},
						{'X','X','X','S','X'}};
	
	char [][] campo11={	{'X','X','X','X','X'},
						{'X',' ',' ',' ','X'},
						{'X',' ',' ',' ','X'},
						{'X','D',' ','H','X'},
						{'X','X','X','S','X'}};
	
	char [][] campo12={	{'X','X','X','X','X'},
						{'X',' ','D',' ','X'},
						{'X',' ',' ',' ','X'},
						{'X',' ',' ','H','X'},
						{'X','X','X','S','X'}};
	
	char [][] campo13={	{'X','X','X','X','X'},
						{'X',' ','D',' ','X'},
						{'X',' ',' ',' ','X'},
						{'X',' ','E','H','X'},
						{'X','X','X','S','X'}};
	
	@Test public void testHeroMoveN() {

		Game g1=new Game(campo,0);
		g1.play('W', 0);
		assertEquals(g1.getHero().getX(),1);
		assertEquals(g1.getHero().getY(),2);
	}
	@Test public void testHeroMoveS() {

		Game g1=new Game(campo,0);
		g1.play('S', 0);
		assertEquals(g1.getHero().getX(),3);
		assertEquals(g1.getHero().getY(),2);
	}
	@Test public void testHeroMoveD() {

		Game g1=new Game(campo,0);
		g1.play('D', 0);
		assertEquals(g1.getHero().getX(),2);
		assertEquals(g1.getHero().getY(),3);
	}
	@Test public void testHeroMoveA() {

		Game g1=new Game(campo,0);
		g1.play('A', 0);
		assertEquals(g1.getHero().getX(),2);
		assertEquals(g1.getHero().getY(),1);
	}
	@Test (expected=IllegalArgumentException.class)
	public void testHeroNotMoveS() {
		Game g1=new Game(campo2,0);
		g1.play('S', 0);
	}
	@Test (expected=IllegalArgumentException.class)
	public void testHeroNotMoveW() {
		Game g1=new Game(campo2,0);
		g1.play('W', 0);
	}
	@Test (expected=IllegalArgumentException.class)
	public void testHeroNotMoveD() {
		Game g1=new Game(campo2,0);
		g1.play('D', 0);
	}
	@Test (expected=IllegalArgumentException.class)
	public void testHeroNotMoveA() {
		Game g1=new Game(campo2,0);
		g1.play('A', 0);
	}
	
	@Test public void testHeroEagleA() {
		Game g1=new Game(campo3,0);
		g1.play('A', 0);
		assertEquals(g1.getEagle().isDead(),true);
		assertEquals(g1.getSpade().isCollected(),true);
		
	}
	@Test public void testHeroEagleD() {
		Game g1=new Game(campo4,0);
		g1.play('D', 0);
		assertEquals(g1.getEagle().isDead(),true);
		assertEquals(g1.getSpade().isCollected(),true);
		
	}
	@Test public void testHeroEagleS() {
		Game g1=new Game(campo5,0);
		g1.play('S', 0);
		assertEquals(g1.getEagle().isDead(),true);
		assertEquals(g1.getSpade().isCollected(),true);
		
	}
	@Test public void testHeroEagleW() {
		Game g1=new Game(campo6,0);
		g1.play('W', 0);
		assertEquals(g1.getEagle().isDead(),true);
		assertEquals(g1.getSpade().isCollected(),true);
		
	}
	@Test (expected=IllegalArgumentException.class)
	public void testHeroNotExitA() {
		Game g1=new Game(campo7,0);
		g1.play('A', 0);
	}
	@Test (expected=IllegalArgumentException.class)
	public void testHeroNotExitS() {
		Game g1=new Game(campo7,0);
		g1.play('S', 0);
	} 
	@Test (expected=IllegalArgumentException.class)
	public void testHeroNotExitD() {
		Game g1=new Game(campo7,0);
		g1.play('D', 0);
	}
	@Test (expected=IllegalArgumentException.class)
	public void testHeroNotExitW() {
		Game g1=new Game(campo7,0);
		g1.play('W', 0);
	}
	
	@Test public void testDragonDeathUp() {
		Game g1=new Game(campo9,0);
		g1.play('A', 0);
		assertEquals(g1.getCampo()[2][2], ' ');
	}
	
	@Test public void testDragonDeathDown() {
		Game g1=new Game(campo13,0);
		g1.play('A', 0);
		g1.play('W', 0);
		assertEquals(g1.getCampo()[1][2], ' ');
	}
	
	@Test public void testDragonDeathRight() {
		Game g1=new Game(campo13,0);
		g1.play('A', 0);
		g1.play('D', 0);
		g1.play('W', 0);
		g1.play('W', 0);
		assertEquals(g1.getCampo()[1][2], ' ');
	}
	
	@Test public void testDragonDeathLeft() {
		Game g1=new Game(campo13,0);
		g1.play('A', 0);
		g1.play('A', 0);
		g1.play('W', 0);
		g1.play('W', 0);
		assertEquals(g1.getCampo()[1][2], ' ');
	}
	
	@Test
	public void testHeroWin() {
		Game g1=new Game(campo10,0);		
		assertTrue(g1.play('W', 0));
		assertTrue(g1.play('A', 0));
		assertTrue(g1.play('D', 0));
		assertTrue(g1.play('S', 0));
		assertFalse(g1.play('S', 0));
	}

	
	@Test
	public void testHeroDeathDown() {
		Game g1=new Game(campo10,0);
		assertTrue(g1.play('A', 0));
		assertFalse(g1.play('A', 0));		
	}
	
	@Test
	public void testHeroDeathRight() {
		Game g1=new Game(campo10,0);
		assertTrue(g1.play('A', 0));
		assertFalse(g1.play('W', 0));		
	}
	
	@Test
	public void testHeroDeathUp() {
		Game g1=new Game(campo11,0);
		assertTrue(g1.play('W', 0));
		assertTrue(g1.play('A', 0));
		assertFalse(g1.play('A', 0));		
	}
	
	@Test
	public void testHeroDeathLeft() {
		Game g1=new Game(campo12,0);
		assertTrue(g1.play('A', 0));
		assertTrue(g1.play('A', 0));
		assertTrue(g1.play('W', 0));
		assertFalse(g1.play('W', 0));		
	}
	
	@Test (expected=IllegalArgumentException.class)
	public void testHeroNotWin() {
		Game g1=new Game(campo10,0);		
		g1.play('S', 0);
	}
	
	
	
	
	
	

	
	

}
