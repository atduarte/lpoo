package maze.test;

import static org.junit.Assert.*;

import maze.logic.Game;

import org.junit.Test;

public class dragons {

	char [][] campo14={	{'X','X','X','X','X','X'},
						{'X',' ',' ',' ',' ','X'},
						{'X','D',' ','E',' ','X'},
						{'X',' ',' ',' ',' ','X'},
						{'X',' ',' ',' ','H','X'},
						{'X','X','X','X','S','X'}};
	
	@Test
	public void testDragonMove() {
		Game g1=new Game(campo14,2);
		g1.play('A', 0, 0, -1, 0);
		assertEquals(g1.getCampo()[1][1], 'D');
	}
	
	@Test
	public void testDragonMoveAndSleep() {
		Game g1=new Game(campo14,2);
		g1.play('A', 0, 0, -1, 1);
		assertEquals(g1.getCampo()[1][1], 'd');
	}
	
	@Test
	public void testDragonModeSpade() {
		Game g1=new Game(campo14,2);
		g1.play('A', 0, 1, 0, 0);
		assertEquals(g1.getCampo()[2][2], 'D');
		g1.play('A', 0, 1, 0, 0);
		assertEquals(g1.getCampo()[2][3], 'F');
		g1.play('A', 0, 1, 0, 0);
		assertEquals(g1.getCampo()[2][3], 'E');
		assertEquals(g1.getCampo()[2][4], 'D');

	}
	
	@Test
	public void testDragonModeAndSleepSpade() {
		Game g1=new Game(campo14,2);
		g1.play('A', 0, 1, 0, 0);
		assertEquals(g1.getCampo()[2][2], 'D');
		g1.play('A', 0, 1, 0, 1);
		assertEquals(g1.getCampo()[2][3], 'f');
		g1.play('A', 0, 0, 0, 0);
		g1.play('D', 0, 1, 0, 0);
		assertEquals(g1.getCampo()[2][3], 'E');
		assertEquals(g1.getCampo()[2][4], 'D');
	}

}
