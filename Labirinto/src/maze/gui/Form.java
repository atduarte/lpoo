package maze.gui;

import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.GridLayout;
import java.awt.LayoutManager;

import javax.swing.AbstractButton;
import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.ButtonModel;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;

import java.awt.BorderLayout;
import javax.swing.JButton;
import javax.swing.border.Border;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import maze.logic.Game;
import maze.logic.Maze;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.io.FileNotFoundException;

/**
 * 
 * Classe que representa a Janel Principal do Modo Grafico
 * 
 * @author Andre Duarte 
 * @author Sergio Esteves 
 *
 */
public class Form {

	
	private static char RIGHT='D';
	private static char EAGLE='G';
	private static int buttonop;
	JFrame MazeFrame;
	private static boolean wantmaze=false;
	private static JPanel mazearea,panel;
	private static JDialog optionPanel;
	private static Maze maze;
	private Game jogo;
	private static char dir;
	private static boolean gamerunning=false;
	private static boolean newgameboolean;
	private int dragonsmove;
	private static int xdimension;
	private int dragonsnumber;
	private static int  eagleHelp=0;
	private static char UP='W';
	private static char DOWN='S';
	private static char LEFT='A';
	private static char [][] campo;
	private static boolean gameOver=false;
	private JFrame frame;
	
	/**
	 * 
	 * Get Frame
	 * 
	 * @return frame
	 */
	public JFrame getFrame() {
		return frame;
	}

	/**
	 * 
	 * Set Frame
	 * 
	 * @param frame
	 */
	public void setFrame(JFrame frame) {
		this.frame = frame;
	}
	
	/**
	 * 
	 * Retorna o campo
	 * 
	 * @return campo
	 */
	public static char[][] getCampo() {
		return campo;
	}

	/**
	 * 
	 * Set campo
	 * 
	 * @param campo
	 */
	public void setCampo(char[][] campo) {
		Form.campo = campo;
	}

	/**
	 * 
	 * Get Xdimension
	 * 
	 * @return dimensao x da matriz
	 */
	public static int getXdimension() {
		return xdimension;
	}

	
	/**
	 * 
	 * Set Xdimension
	 * 
	 * @param xdimension
	 */
	public void setXdimension(int xdimension) {
		Form.xdimension = xdimension;
	}

	
	/**
	 * 
	 * Retorna o numero de dragoes
	 * 
	 * @return numero de dragoes
	 */
	public int getDragonsnumber() {
		return dragonsnumber;
	}

	/**
	 * 
	 * Set Numero de Dragoes
	 * 
	 * @param dragonsnumber
	 */
	public void setDragonsnumber(int dragonsnumber) {
		this.dragonsnumber = dragonsnumber;
	}
	
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Form window = new Form();
					window.frame.setVisible(true);
					window.frame.setLocationRelativeTo(null);
					window.frame.setResizable(false);
					//JScrollPane scrolltxt = new JScrollPane(txt);
					//window.frame.



				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Form() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();



		frame.addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent arg0) {
				exit();		
			}
		});
		frame.setBounds(100, 100, 500, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(new BorderLayout(0, 0));


		JPanel panel = new JPanel();
		frame.getContentPane().add(panel,BorderLayout.SOUTH);

		JButton btnNewGame = new JButton("New Game");
		btnNewGame.setFocusable(false);
		btnNewGame.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				newgameboolean=false;
				String[] items = {"Dragoes Parados","Dragoes A Andar","Dragoes em Accao"};
				final JComboBox<String> dragonscombo = new JComboBox<String>(items);
				final JTextField field1 = new JTextField("10");
				final JTextField field2 = new JTextField("2");
				JPanel inputpanel = new JPanel(new GridLayout(0, 1));
				inputpanel.add(dragonscombo);
				inputpanel.add(new JLabel("X dimension:"));
				inputpanel.add(field1);
				inputpanel.add(new JLabel("Dragon Number:"));
				inputpanel.add(field2);

				JRadioButton btnautomaze = new JRadioButton("Automatic Maze");
				btnautomaze.setSelected(true);
				JRadioButton btncreatemaze = new JRadioButton("Create Maze");
				JRadioButton btndefaultmaze = new JRadioButton("Default Maze");

				ButtonGroup group = new ButtonGroup();
				group.add(btnautomaze);
				group.add(btncreatemaze);
				group.add(btndefaultmaze);

				inputpanel.add(btnautomaze);
				inputpanel.add(btncreatemaze);
				inputpanel.add(btndefaultmaze);

				btnautomaze.addChangeListener(new ChangeListener() {
					public void stateChanged(ChangeEvent changEvent) {
						AbstractButton aButton = (AbstractButton)changEvent.getSource();
						ButtonModel aModel = aButton.getModel();
						boolean selected = aModel.isSelected();

						if(selected) {
							field1.setEnabled(true);
							field2.setEnabled(true);
						}
					}
				});

				btncreatemaze.addChangeListener(new ChangeListener() {
					public void stateChanged(ChangeEvent changEvent) {
						AbstractButton aButton = (AbstractButton)changEvent.getSource();
						ButtonModel aModel = aButton.getModel();
						boolean selected = aModel.isSelected();

						if(selected) {
							field1.setEnabled(true);
							field2.setEnabled(false);
						}
					}
				});





				btndefaultmaze.addChangeListener(new ChangeListener() {
					public void stateChanged(ChangeEvent changEvent) {
						AbstractButton aButton = (AbstractButton)changEvent.getSource();
						ButtonModel aModel = aButton.getModel();
						boolean selected = aModel.isSelected();

						if(selected) {
							field1.setText("10");
							field2.setText("1");
							field1.setEnabled(false);
							field2.setEnabled(false);
						}
					}
				});

				int result = JOptionPane.showConfirmDialog(null, inputpanel, "Configuracoes",JOptionPane.OK_CANCEL_OPTION, JOptionPane.PLAIN_MESSAGE);
				if (result == JOptionPane.OK_OPTION) {
					//Ler os valores
					//Testar inputs
					gameOver=false;
					eagleHelp=0;
					dragonsmove=dragonscombo.getSelectedIndex();
					xdimension=Integer.parseInt(field1.getText());
					dragonsnumber=Integer.parseInt(field2.getText());
					newgameboolean=true;


					if(btnautomaze.isSelected()) {

						// GERAR AUTOMATICAMENTE
						maze = new Maze();
						System.out.println(xdimension);
						maze.generate(xdimension);


						for(int i = 0; i < maze.getCampo().length; i++) {
							for(int j = 0; j < maze.getCampo().length; j++) {
								System.out.print(maze.getCampo()[i][j]);
								if(j==(maze.getCampo().length-1))
									System.out.println();
							}
						}

						frame.setSize(xdimension*50, xdimension*50);
						frame.setLocationRelativeTo(null);
						maze.positioningElements(dragonsnumber);

						jogo=new Game(maze.getCampo(),dragonsmove);
						createMaze();
						mazearea.requestFocusInWindow();

						play();

					} else if(btncreatemaze.isSelected()) {


						// CRIAR LABIRINTO
						createmazeuser();







					} else if(btndefaultmaze.isSelected()){

						// DEFAULT
						dragonsmove=dragonscombo.getSelectedIndex();
						frame.setSize(10*50, 10*50);
						frame.setLocationRelativeTo(null);
						jogo=new Game(true, dragonsmove);
						createMaze();
						mazearea.requestFocusInWindow();

						play();
					}



					//System.out.println(newgameboolean);

				} else {
					System.out.println("Cancelled");
				}
			}


		});
		panel.add(btnNewGame);

		JButton btnNewButton = new JButton("Exit");
		btnNewButton.setFocusable(false);
		btnNewButton.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				exit();
			}
		});
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			}
		});

		JButton btnNewButton_1 = new JButton("Options");
		btnNewButton_1.setFocusable(false);
		btnNewButton_1.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				System.out.println("Entrou");
				if(optionPanel.isVisible()==false)
				{
					optionPanel.setVisible(true);
					optionPanel.setLocationRelativeTo(null);
					optionPanel.requestFocusInWindow();

				}
				else if(!((UP==RIGHT)||(UP==LEFT)||(UP==DOWN)||(UP==EAGLE)||(DOWN==RIGHT)||(DOWN==LEFT)||(DOWN==EAGLE)||(RIGHT==EAGLE)|| (RIGHT==LEFT) || (RIGHT==EAGLE) || (LEFT==EAGLE)))
				{
					optionPanel.setVisible(false);
					if (mazearea != null)mazearea.requestFocusInWindow();
				}
			}
		});
		panel.add(btnNewButton_1);
		panel.add(btnNewButton);

		optionPanel = new JDialog();

		optionPanel.setVisible(false);
		optionPanel.setBounds(0,0,570,330);
		optionPanel.getContentPane().setLayout(null);
		optionPanel.setLocationRelativeTo(null);


		JButton btnNewButton_2 = new JButton("SAVE GAME");
		btnNewButton_2.setFocusable(false);
		btnNewButton_2.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				if(gamerunning)
				{
					jogo.save();
					JOptionPane.showMessageDialog(null, "Jogo guardado com sucesso");

				}
				else
				{
					JOptionPane.showMessageDialog(null, "Necessita de ter um jogo em curso para poder guardar");
				}
			}
		});
		btnNewButton_2.setBounds(10, 32, 103, 23);
		optionPanel.getContentPane().add(btnNewButton_2);

		JButton btnNewButton_3 = new JButton("LOAD GAME");
		btnNewButton_3.setFocusable(false);
		btnNewButton_3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if(!((UP==RIGHT)||(UP==LEFT)||(UP==DOWN)||(UP==EAGLE)||(DOWN==RIGHT)||(DOWN==LEFT)||(DOWN==EAGLE)||(RIGHT==EAGLE)|| (RIGHT==LEFT) || (RIGHT==EAGLE) || (LEFT==EAGLE)))
				{
					try {
						jogo=new Game();
						jogo.load();
						
					} catch(Exception e) {
						JOptionPane.showMessageDialog(null, "N�o foi poss�vel carregar o ficheiro");
						return;
					}
					jogo.recognizeElements();
					frame.setLocationRelativeTo(null);
					optionPanel.setVisible(false);
					xdimension=jogo.getCampo().length;
					frame.setSize(xdimension*50, xdimension*50);
					frame.setLocationRelativeTo(null);
					createMaze();
					mazearea.requestFocusInWindow();
					play();
				}
				else
				{
					JOptionPane.showMessageDialog(null, "As teclas utilizadas tem de ser todas diferentes");
				}
			}
		});
		btnNewButton_3.setBounds(10, 66, 103, 23);
		optionPanel.getContentPane().add(btnNewButton_3);

		JLabel lblConfiguracoes = new JLabel("Configuracoes");
		lblConfiguracoes.setBounds(243, 11, 95, 14);
		optionPanel.getContentPane().add(lblConfiguracoes);

		final JButton btnDown = new JButton("S");
		btnDown.setFocusable(false);
		btnDown.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				optionPanel.requestFocusInWindow();


				buttonop=1;

			}
		});
		btnDown.setBounds(181, 66, 103, 23);
		optionPanel.getContentPane().add(btnDown);

		final JButton btnRight = new JButton("D");
		btnRight.setFocusable(false);
		btnRight.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				optionPanel.requestFocusInWindow();

				buttonop=2;
			}
		});
		btnRight.setBounds(181, 100, 103, 23);
		optionPanel.getContentPane().add(btnRight);

		final JButton btnLeft = new JButton("A");
		btnLeft.setFocusable(false);
		btnLeft.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				optionPanel.requestFocusInWindow();

				buttonop=3;
			}
		});


		btnLeft.setBounds(181, 134, 103, 23);
		optionPanel.getContentPane().add(btnLeft);

		final JButton btnEagle = new JButton("G");
		btnEagle.setFocusable(false);
		btnEagle.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {

				optionPanel.requestFocusInWindow();

				buttonop=4;
			}
		});
		btnEagle.setBounds(181, 168, 103, 23);
		optionPanel.getContentPane().add(btnEagle);

		final JButton btnUp = new JButton("W");
		btnUp.setFocusable(false);
		btnUp.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				optionPanel.requestFocusInWindow();

				buttonop=0;

			}
		});
		btnUp.setBounds(181, 32, 103, 23);
		optionPanel.getContentPane().add(btnUp);

		JLabel lblNewLabel = new JLabel("UP");
		lblNewLabel.setBounds(363, 36, 46, 14);
		optionPanel.getContentPane().add(lblNewLabel);

		JLabel lblNewLabel_1 = new JLabel("DOWN");
		lblNewLabel_1.setBounds(363, 70, 46, 14);
		optionPanel.getContentPane().add(lblNewLabel_1);

		JLabel lblNewLabel_2 = new JLabel("RIGHT");
		lblNewLabel_2.setBounds(363, 104, 46, 14);
		optionPanel.getContentPane().add(lblNewLabel_2);

		JLabel lblNewLabel_3 = new JLabel("LEFT");
		lblNewLabel_3.setBounds(363, 138, 46, 14);
		optionPanel.getContentPane().add(lblNewLabel_3);

		JLabel lblNewLabel_4 = new JLabel("EAGLE");
		lblNewLabel_4.setBounds(363, 172, 46, 14);
		optionPanel.getContentPane().add(lblNewLabel_4);

		JButton btnNewButton_8 = new JButton("Guardar Configuracoes");
		btnNewButton_8.setFocusable(false);
		btnNewButton_8.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {

				if(!((UP==RIGHT)||(UP==LEFT)||(UP==DOWN)||(UP==EAGLE)||(DOWN==RIGHT)||(DOWN==LEFT)||(DOWN==EAGLE)||(RIGHT==EAGLE)|| (RIGHT==LEFT) || (RIGHT==EAGLE) || (LEFT==EAGLE)))
				{
					optionPanel.setVisible(false);
					if (mazearea != null) mazearea.requestFocusInWindow();
				}
				else
				{
					JOptionPane.showMessageDialog(null, "As teclas utilizadas tem de ser todas diferentes");
				}
			}
		});
		btnNewButton_8.setBounds(259, 197, 170, 23);


		optionPanel.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent arg0) {
				if(buttonop==0)
				{
					System.out.println(arg0.getKeyCode());

					if((arg0.getKeyCode()>=48 && arg0.getKeyCode()<=57) || (arg0.getKeyCode()>=65 &&arg0.getKeyCode()<=90))
					{
						btnUp.setText(""+Character.toUpperCase(arg0.getKeyChar()));
						UP=btnUp.getText().charAt(0);
					}
				}
				else if (buttonop==1)
				{
					if((arg0.getKeyCode()>=48 && arg0.getKeyCode()<=57) || (arg0.getKeyCode()>=65 &&arg0.getKeyCode()<=90))
					{
						btnDown.setText(""+Character.toUpperCase(arg0.getKeyChar()));
						DOWN=btnDown.getText().charAt(0);
					}
				}
				else if (buttonop==2)
				{
					if((arg0.getKeyCode()>=48 && arg0.getKeyCode()<=57) || (arg0.getKeyCode()>=65 &&arg0.getKeyCode()<=90))
					{
						btnRight.setText(""+Character.toUpperCase(arg0.getKeyChar()));
						RIGHT=btnRight.getText().charAt(0);
					}
				}
				else if (buttonop==3)
				{
					if((arg0.getKeyCode()>=48 && arg0.getKeyCode()<=57) || (arg0.getKeyCode()>=65 &&arg0.getKeyCode()<=90))
					{
						btnLeft.setText(""+Character.toUpperCase(arg0.getKeyChar()));
						LEFT=btnLeft.getText().charAt(0);
					}
				}
				else if(buttonop==4)
				{
					if((arg0.getKeyCode()>=48 && arg0.getKeyCode()<=57) || (arg0.getKeyCode()>=65 &&arg0.getKeyCode()<=90))
					{
						btnEagle.setText(""+Character.toUpperCase(arg0.getKeyChar()));
						EAGLE=btnEagle.getText().charAt(0);
						System.out.println(EAGLE);
					}
				}
			}
		});

		optionPanel.getContentPane().add(btnNewButton_8);
	}

	private void createMaze() {

		if (mazearea != null) frame.getContentPane().remove(mazearea);
		mazearea=new JPanel(new GridLayout(xdimension, xdimension));
		//mazearea.removeAll();

		//mazearea.updateUI();
		mazearea.setBorder(null);
		mazearea.setBackground(Color.WHITE);

		for(int i=0;i<xdimension;i++)
		{
			for(int j=0;j<xdimension;j++)
			{

				JLabel l = new JLabel(""+jogo.getCampo()[i][j]+""+jogo.getCampoAguia()[i][j], JLabel.CENTER);
				l.setSize(50,50);
				l.setBorder( BorderFactory.createLineBorder(Color.BLACK) );
				mazearea.add(l);


			}
		}
		mazearea.addKeyListener(new KeyListener() {
			@Override
			public void keyPressed(KeyEvent arg0) {
				// TODO Auto-generated method stub


				if(!gameOver && arg0.getKeyChar()!=EAGLE)
				{
					System.out.println("Entrou");
					dir=arg0.getKeyChar();
					dir=Character.toUpperCase(dir);
					if(dir==UP)
					{
						dir='W';
					}else if(dir==DOWN)
					{
						dir='S';
					}else if(dir==RIGHT)
					{
						dir='D';
					}else if(dir==LEFT)
					{
						dir='A';
					} 
					else if(dir==EAGLE)
					{
						eagleHelp=1;
					}
					else
					{
						dir='z';
					}
					System.out.println(dir);
					try {
						if(!jogo.play(dir,eagleHelp))
						{
							gameOver=true;
						} 
						else
						{

							gameOver=false;


						}
						//Morreu ou ganhou
					} catch(IllegalArgumentException e) {
						System.out.println("Direc��o Inv�lida.");

					}
					play();

					frame.validate();
				}else if(arg0.getKeyChar()==EAGLE)
				{
					eagleHelp=1;
				}

			}

			@Override
			public void keyReleased(KeyEvent arg0) {
				// TODO Auto-generated method stub

			}

			@Override
			public void keyTyped(KeyEvent arg0) {
				// TODO Auto-generated method stub

			}

		});

		frame.getContentPane().add(mazearea, BorderLayout.CENTER);
		frame.validate();


	}

	/**
	 * Funcao responsavel por verificar as jogadas
	 */
	public void play() {
		gamerunning=true;
		draw();
		System.out.println("Entrou no PLAY");
		if(!gameOver) 
		{
			System.out.println("Entrou no ciclo play");
			//gameOver=update();
			draw();
			frame.validate();
		}  
		else
		{
			System.out.println("Entrou no PLAY");
			eagleHelp=0;
			gamerunning=false;
			wantmaze=false;
			if(jogo.getSpade().isCollected())
			{
				JOptionPane.showMessageDialog(null, "GANHOU!");
			}
			else
			{
				JOptionPane.showMessageDialog(null, "PERDEU!");
				draw();

			}
			mazearea.removeAll();
			mazearea.repaint();
			mazearea.setBackground(Color.GRAY);
			//frame.validate();
		}


		frame.validate();


		// end game!
	}



	//exit option

	/**
	 * Funcao para sair
	 */
	private void exit() {
		int option=JOptionPane.showConfirmDialog(null, "Deseja mesmo Sair?","Exit",JOptionPane.YES_NO_OPTION,JOptionPane.WARNING_MESSAGE);
		if(option==JOptionPane.YES_OPTION)
		{
			System.exit(0);
		}
	}




/**
 * Funcao que desenha a matriz
 */
	public void draw()
	{
		System.out.println("Entrou no draw");
		for(int i=0;i<xdimension;i++)
		{
			for(int j=0;j<xdimension;j++)
			{



				((JLabel)mazearea.getComponent(i*xdimension+j)).setText(""+jogo.getCampo()[i][j]+""+jogo.getCampoAguia()[i][j]);



			}
		}


	}
/**
 * Funcao que cria a matriz
 */
	public void creategame()
	{
		System.out.println("Entrou");
		//verificar antes
		frame.setSize(xdimension*50, xdimension*50);
		frame.setLocationRelativeTo(null);
		maze = new Maze();
		maze.setCampo(campo);
		System.out.println("numero de dragoes:"+ dragonsnumber+"movimento"+dragonsmove );
		jogo=new Game(maze.getCampo(),dragonsmove);
		createMaze();
		mazearea.requestFocusInWindow();
		play();
	}
	/** 
	 * Funcao que cria a matriz definida pelo utilizador
	 */
	public void createmazeuser()
	{
		frame.setVisible(false);
		JFrame MazeFrame= new FormMaze(this,xdimension, dragonsnumber);
		MazeFrame.setVisible(true);
		MazeFrame.setLocationRelativeTo(null);
		MazeFrame.setSize(xdimension*50, xdimension*50);
	}
}