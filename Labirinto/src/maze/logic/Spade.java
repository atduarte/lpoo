package maze.logic;

import java.io.Serializable;

/**
 * 
 * Classe representativa da entidade Espada
 * 
 * @author Andre Duarte 
 * @author Sergio Esteves 
 *
 */
public class Spade extends GameElement implements Serializable {
	
	private static final long serialVersionUID = 1L;
	private boolean collected = false;
	private boolean eagleCollected = false;

	
	/**
	 * 
	 * Get da variavel eagleCollected
	 * 
	 * @return true se a aguia contem a espada ou falso no caso contrario
	 */
	public boolean isEagleCollected() {
		return eagleCollected;
	}

	
	/**
	 * 
	 * Set da variavel eagleCollected
	 * 
	 * @param eagleCollected true se a aguia contem a espada ou falso no caso contrario
	 */
	public void setEagleCollected(boolean eagleCollected) {
		this.eagleCollected = eagleCollected;
	}

	/**
	 * 
	 * Get da variavel collected
	 * 
	 * @return true se o heroi apanhou a espada ou falso no caso contrario
	 */
	public boolean isCollected() {
		return collected;
	}

	/**
	 * 
	 * Set da variavel collected
	 * 
	 * @param collected true se o heroi apanhou a espada ou falso no caso contrario
	 */
	public void setCollected(boolean collected) {
		this.collected = collected;
	}
	
}
