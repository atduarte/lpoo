package maze.logic;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Stack;



/**
 * 
 * Classe que implementa a l�gica do Labirinto
 * 
 * @author Andre Duarte
 * @author Sergio Esteves
 *
 */
public class Maze {

	int n;
	Stack<int[]> path = new Stack<int[]>();
	char [][] campo;
	Random r = new Random();
	
	public static final char cWall 						= 'X';
	public static final char cHero 						= 'H';
	public static final char cHeroArmed 				= 'A';
	public static final char cDragon					= 'D';
	public static final char cDragonSleeping			= 'd';
	public static final char cDragonSpade				= 'F';
	public static final char cDragonSpadeSleeping		= 'f';
	public static final char cSpade 					= 'E';
	public static final char cSpadeEagle				= 'e';
	public static final char cEagle 					= 'a';
	public static final char cEagleFlying				= 'a';
	public static final char cExit						= 'S';
	public static final char cSpace						= ' ';



	/**
	*
	*Funcao que gera o labirinto
	*@param N dimensao do labirinto
	*@return true se possivel falso no caso contrario
	*
	*/
	public boolean generate(int N) {

		if(N < 5)
			return false;

		n = N;

		campo = new char[n][n];
		for(int i = 0; i < n; i++)
			for(int j = 0; j < n; j++)
				campo[i][j] = 'X';

		randomExit();

		int error = n*n;
		while(existemBlocosPretos()) {
			percorre();
			error--;
			if(error == 0) {
					return generate(n);
			}
		}
		
		while(percorre()) { }

		return true;
	}

	/**
	*
	*Funcao que verifica se existem caminhos que respeitem as indicacoes para um labirinto possivel
	*@return true se possivel falso no caso contrario 
	*
	*/
	public boolean verificaCaminhos() {
		
		n = campo.length;
		
		if(!checkAllDiagonais()) 
			return false;
		
		if(existemBlocosPretos())
			return false;
		
		if(!checkAllBlocosBrancos()) 
			return false;
		
		// Procurar a saida
		int[] tmp = new int[2];
		tmp[0] = 0;
		tmp[1] = 0;
		
		for(int i = 0; i < n-1; i++) {
			
			if(campo[i][0] == cExit) {
				tmp[0] = i;
				tmp[1] = 0;
				break;
			}
			else if(campo[i][n-1] == cExit) {
				tmp[0] = i;
				tmp[1] = n-1;
				break;
			}
			else if(campo[0][i] == cExit) {
				tmp[0] = 0;
				tmp[1] = i;
				break;
			}
			else if(campo[n-1][i] == cExit) {
				tmp[0] = n-1;
				tmp[1] = i;
				break;
			}

		}
		
		// Verificar casa junto � saida
		char [][] campoTemp = campo;		
		Stack<int[]> testPath = new Stack<int[]>();
		
		if(tmp[0] == 0)
			tmp[0]++;
		else if(tmp[0] == n-1)
			tmp[0]--;
		else if(tmp[1] == 0)
			tmp[1]++;
		else if(tmp[1] == n-1)
			tmp[1]--;
		
		if(campoTemp[tmp[0]][tmp[1]] != ' ')
			return false;
		
		testPath.add(tmp);	
		campoTemp[tmp[0]][tmp[1]] = '.';
		
		while(!testPath.empty()) {
			
			List<String> direcoes = new ArrayList<String>();
			direcoes.add("c");
			direcoes.add("d");
			direcoes.add("b");
			direcoes.add("e");
			
			while(direcoes.size() > 0) {
				
				int tmpR = r.nextInt(direcoes.size());
				
				int [] tmp2 = new int[2];
				
				if(direcoes.get(tmpR).equals("d")){
					tmp2[0] = tmp[0];
					tmp2[1] = tmp[1]+1;				
				} else if(direcoes.get(tmpR).equals("b")){
					tmp2[0] = tmp[0]+1;
					tmp2[1] = tmp[1];
				} else if(direcoes.get(tmpR).equals("e")){
					tmp2[0] = tmp[0];
					tmp2[1] = tmp[1]-1;
				} else if(direcoes.get(tmpR).equals("c")){
					tmp2[0] = tmp[0]-1;
					tmp2[1] = tmp[1];
				}
				
				if(campoTemp[tmp2[0]][tmp2[1]] == ' ') {
					campoTemp[tmp2[0]][tmp2[1]] = '.';
					testPath.add(tmp2);
					tmp = tmp2;
					break;
				} 
				
				direcoes.remove(direcoes.get(tmpR));
				
				if(direcoes.size() == 0) {
					testPath.pop();
					if(!testPath.empty())
						tmp = testPath.peek();
					break;
				}
				
			}		
		}
		
		// Percorre a ver se h� sitios com espa�os
		for(int i = 0; i < n-1; i++)
			for(int j = 0; j < n-1; j++)
				if(campoTemp[i][j] == ' ')
					return false;	
		
		return true;
		
	}
	
	private boolean percorre() {
		
		if(path.empty())
			return false;
		
		int [] c = path.peek();
		List<String> direcoes = new ArrayList<String>();
		direcoes.add("c");
		direcoes.add("d");
		direcoes.add("b");
		direcoes.add("e");

		while(direcoes.size() > 0) {
			int tmp = r.nextInt(direcoes.size());
			
			int [] c2 = new int[2];
			if(direcoes.get(tmp).equals("d")){
				c2[0] = c[0];
				c2[1] = c[1]+1;				
			} else if(direcoes.get(tmp).equals("b")){
				c2[0] = c[0]+1;
				c2[1] = c[1];
			} else if(direcoes.get(tmp).equals("e")){
				c2[0] = c[0];
				c2[1] = c[1]-1;
			} else if(direcoes.get(tmp).equals("c")){
				c2[0] = c[0]-1;
				c2[1] = c[1];
			}	
		
			if(campo[c2[0]][c2[1]] == 'X' && c2[0] != 0 && c2[1] != 0 && c2[0] != n-1 && c2[1] != n-1 && checkBlocosBrancos(c2) && checkDiagonais(c2)) {
				path.add(c2);
				campo[c2[0]][c2[1]] = ' ';
				return true;
			} else {
				direcoes.remove(direcoes.get(tmp));
			}
		}

		path.pop();
		return percorre();
		 
	}

	private boolean checkBlocosBrancos(int [] c) {
		
		/*if(campo[c[0]][c[1]] != ' ')
			return true;*/

		if(campo[c[0]+1][c[1]] == ' ') {
			if(campo[c[0]][c[1]-1] == ' ' && campo[c[0]+1][c[1]-1] == ' ')
				return false;
			if(campo[c[0]][c[1]+1] == ' ' && campo[c[0]+1][c[1]+1] == ' ')
				return false;
		}

		if(campo[c[0]-1][c[1]] == ' ') {
			if(campo[c[0]][c[1]-1] == ' ' && campo[c[0]-1][c[1]-1] == ' ')
				return false;
			if(campo[c[0]][c[1]+1] == ' ' && campo[c[0]-1][c[1]+1] == ' ')
				return false;
		}

		return true;


	}

	private boolean checkDiagonais(int [] c) {
		
		if(campo[c[0]-1][c[1]-1] == ' ') {
			if(campo[c[0]-1][c[1]] != ' ' && campo[c[0]][c[1]-1] != ' ')
				return false;
		} 
		if(campo[c[0]+1][c[1]-1] == ' ') {
			if(campo[c[0]+1][c[1]] != ' ' && campo[c[0]][c[1]-1] != ' ')
				return false;
		} 
		if(campo[c[0]+1][c[1]+1] == ' ') {
			if(campo[c[0]+1][c[1]] != ' ' && campo[c[0]][c[1]+1] != ' ')
				return false;
		} 
		if(campo[c[0]-1][c[1]+1] == ' ') {
			if(campo[c[0]-1][c[1]] != ' ' && campo[c[0]][c[1]+1] != ' ')
				return false;
		}
		return true;

	}

	private boolean checkAllDiagonais() {
		for(int i = 1; i < campo.length-1; i++)
			for(int j = 1; j < campo.length-1; j++) {
				int[] tmp = new int[2];
				tmp[0] = i;
				tmp[1] = j; 
				
				if(campo[tmp[0]][tmp[1]]==' ' && !checkDiagonais(tmp))
					return false;
			}
		return true;
	}
	
	private boolean checkAllBlocosBrancos() {
		for(int i = 1; i < campo.length-1; i++)
			for(int j = 1; j < campo.length-1; j++) {
				int[] tmp = new int[2];
				tmp[0] = i;
				tmp[1] = j;
				if(campo[tmp[0]][tmp[1]]==' ' && !checkBlocosBrancos(tmp)) {
					return false;
				}
			}
		return true;
	}
	
	private boolean existemBlocosPretos() {
		for(int i = 0; i < n-2; i++)
			for(int j = 0; j < n-2; j++)
				if(campo[i][j] == 'X' && campo[i+1][j] == 'X' && campo[i+2][j] == 'X' &&
				campo[i][j+1] == 'X' && campo[i+1][j+1] == 'X' && campo[i+2][j+1] == 'X' &&
				campo[i][j+2] == 'X' && campo[i+1][j+2] == 'X' && campo[i+2][j+2] == 'X')
					return true;
		return false;
	}

	private boolean randomExit() {
		int sX = -1, sY = -1;

		int tmp = r.nextInt(4);

		if(tmp == 0)
			sX = 0;
		else if(tmp == 1)
			sX = n-1;
		else if(tmp == 2)
			sY = 0;
		else if(tmp == 3)
			sY = n-1;

		if(sX == -1)
			do{
				sX = r.nextInt(n);
			} while(sX == 0 || sX == n-1);

		if(sY == -1)
			do{
				sY = r.nextInt(n);
			} while(sY == 0 || sY == n-1);

		campo[sX][sY] = 'S';

		if(sX == n-1) sX--;
		else if(sX == 0) sX++;
		else if(sY == 0) sY++;
		else if(sY == n-1) sY--;

		campo[sX][sY] = ' ';

		int [] tmpArray = new int[2];
		tmpArray[0] = sX;
		tmpArray[1] = sY;
		path.add(tmpArray);
		
		return true;
	}


/**
	 * 
	 * Retorna o campo
	 * 
	 * @return campo
	 */
	public char[][] getCampo() {
		return campo;
	}
	

	/**
	 * 
	 * Set campo
	 * 
	 * @param Campo
	 */
	public void setCampo(char[][] Campo) {
		campo = Campo;
	}
	


	



	/**
	*
	*Funcao que conta o numero de espacos em branco de um labirinto
	*@return Numero de espacos em branco
	*/
	public int countFreeSpaces() {
		int k = 0;
		
		for(int i = 0; i < n; i++)
			for(int j = 0; j < n; j++)
				if(campo[i][j] == ' ')
					k++;
		return k;
		
	}

	/**
	*
	*Funcao que posiciona os elementos de forma aleatorio no campo
	*@param nDragons Numero de dragoes a posicionar
	*
	*/
	public void positioningElements(int nDragons) {
		Random r = new Random();
		int x, y;

		// Heroi com Aguia

		do {
			x=r.nextInt(n);
			y=r.nextInt(n);
		} while(campo[x][y]!=' ');

		campo[x][y] = cHero;
		
		// Spade 

		do{ 
			x=r.nextInt(n);
			y=r.nextInt(n);
		} while(campo[x][y]!=' ');
		campo[x][y] = cSpade;	
		

		// Dragons

		for(int i = 0; i < nDragons; i++) {
			if(countFreeSpaces() == 0)
				break;
			
			do{
				x=r.nextInt(n);
				y=r.nextInt(n);
			}while(campo[x][y]!=' ');

			campo[x][y]= cDragon;
		}

		
	}
}

