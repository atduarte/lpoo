package maze.logic;

import java.io.Serializable;

/**
 * 
 * Classe representativa da entidade gen�rica Elemento
 * 
 * @author Andre Duarte 
 * @author Sergio Esteves
 * @see Serializable
 *
 */
public class GameElement implements Serializable {

	private static final long serialVersionUID = 1L;
	private int x;
	private int y;
	
	/**
	 * Construtor 
	 * @param X
	 * @param Y
	 */
	public GameElement(int X, int Y) {
		x = X;
		y = Y;
	}
	
	/**
	 * Construtor por defeito
	 */
	public GameElement() {
		
	}
	/**
	 * 
	 * Get posicaoX
	 * 
	 * @return posX
	 */
	public int getX() {
		return x;
	}
	/**
	 * 
	 * Set posicao X
	 * 
	 * @param x
	 */
	public void setX(int x) {
		this.x = x;
	}
	/**
	 * 
	 * Get posicao Y
	 * 
	 * @return gposY
	 */
	public int getY() {
		return y;
	}
	/**
	 * 
	 * Set posicao Y
	 * 
	 * @param y
	 */
	public void setY(int y) {
		this.y = y;
	}	
}
