package com.lpoo.teeter.models;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;


public class Game {

	public int campo[][];

    public Ball ball = null;

    public ArrayList<Coord> walls;

    public Game(InputStream is) throws IOException
	{
		campo = (new Reader()).readerInput(is);

        for(int i = 0; i < campo.length; i++)
            for(int j = 0; j < campo[i].length; j++)
                if(campo[i][j] == 3)
                    ball = new Ball(i*30, j*30);

        transform();
	}

    private void transform()
    {
        walls  = new ArrayList<Coord> ();
        for(int i=0;i<campo.length;i++)
        {
            for(int j=0;j<campo[i].length;j++)
            {
                // y - j
                if(campo[i][j]==1)
                    walls.add(new Coord(j*30,(j+1)*30,i*30,(i+1)*30));
            }
        }

        boolean encontrou=true;

        while(encontrou)
        {
            encontrou=false;
            for(int i=0;i<walls.size();i++)
            {
                for(int j=0;j<walls.size();j++)
                {
                    if(walls.get(j).getXi()==walls.get(i).getXf() && i!=j && walls.get(i).getYi()==walls.get(j).getYi() && walls.get(i).getYf()==walls.get(j).getYf())
                    {
                        walls.get(i).setXf(walls.get(i).getXf()+30);
                        walls.remove(j);
                        j--;
                        encontrou=true;
                    }

                    if(walls.get(i).getYf()==walls.get(j).getYi() && i!=j && walls.get(i).getXi()==walls.get(j).getXi() &&  walls.get(i).getXf()==walls.get(j).getXf())
                    {
                        walls.get(i).setYf(walls.get(i).getYf()+30);
                        walls.remove(j);
                        j--;
                        encontrou=true;
                    }
                }
            }
        }

        walls.add(new Coord(-10, 0, 0, 540));
        walls.add(new Coord(960, 960, 0, 540));
        walls.add(new Coord(0, 960, -10, 0));
        walls.add(new Coord(0, 960, 540, 540));
    }

}
