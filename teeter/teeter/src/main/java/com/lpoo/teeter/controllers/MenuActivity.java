package com.lpoo.teeter.controllers;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.lpoo.teeter.R;
import com.lpoo.teeter.core.FullscreenActivity;


public class MenuActivity extends FullscreenActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Set Content
        setContentView(R.layout.activity_fullscreen);

        // final View controlsView = findViewById(R.id.fullscreen_content_controls);
        //final View menuView = findViewById(R.id.menu_content);

        // Play Button

        final Button play_button = (Button) findViewById(R.id.play_button);

        play_button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent(MenuActivity.this, PlayActivity.class);
                startActivity(intent);
            }
        });

        // Info Button

        final Button info_button = (Button) findViewById(R.id.info_button);

        final TextView info_content = (TextView) findViewById(R.id.info_content);
        final ImageView info_overlay = (ImageView) findViewById(R.id.info_overlay);

        info_button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                info_overlay.setVisibility(ImageView.VISIBLE);
                info_content.setVisibility(TextView.VISIBLE);

                info_overlay.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        info_overlay.setVisibility(ImageView.INVISIBLE);
                        info_content.setVisibility(TextView.INVISIBLE);
                    }
                });
            }
        });


    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);

    }

}
