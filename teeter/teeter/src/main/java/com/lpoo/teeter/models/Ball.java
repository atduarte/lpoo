package com.lpoo.teeter.models;

import android.graphics.Point;

import java.util.ArrayList;

public class Ball {

    float x = 0, y = 0;
    float xOld = 0, yOld = 0;
    float xSpeed = 0, ySpeed = 0;

    float friction = (float)0.985;

    float reverseSpeed = (float)-0.8;
    float slowSpeed =(float) 0.98;

    int fps = 60;

    float gravity = (float)(0.2 * 30.0/fps);

    float ax = 0, ay = 0;

    private Point intersect;

    public Ball(float X, float Y) {
        x = X;
        y = Y;
    }

    public void setFps(int Fps) {
        fps = Fps;
    }

    public void setAx(float Ax) {
        ax = Ax;
    }

    public void setAy(float Ay) {
        ay = Ay;
    }

    public void move(ArrayList<Coord> walls) {
        xOld = x;
        yOld = y;

        x += xSpeed;
        y += ySpeed;

        // Ressalto em Paredes
                Boolean colisionX = false, colisionY = false;
                for(int i = 0; i < walls.size(); i++) {

                    // Verificar colisão em X
                    if(!colisionX && y+30 > walls.get(i).getYi() && y < walls.get(i).getYf()) {
                        if(xOld <= walls.get(i).getXi() && x+30 > walls.get(i).getXi()) {
                            hitX();
                            x = walls.get(i).getXi()-30;
                            colisionX = true;
                        } else if (xOld >= walls.get(i).getXf() && x < walls.get(i).getXf()) {
                            hitX();
                            x = walls.get(i).getXf();
                            colisionX = true;
                        }
                    }

                    // Verificar colisão em Y
                    if(!colisionY && x+30 activity_fullscreen.xml> walls.get(i).getXi() && x < walls.get(i).getXf()) {
                        if(yOld <= walls.get(i).getYi() && y+30 > walls.get(i).getYi()) {
                            hitY();
                            y = walls.get(i).getYi()-30;
                            colisionY = true;
                        } else if (yOld >= walls.get(i).getYf() && y < walls.get(i).getYf()) {
                            hitY();
                            y = walls.get(i).getYf();
                            colisionY = true;
                        }
            }

           /* if(colisionX && colisionY)
                break;*/
        }

        /*if(x < 0 || x > 960-30) {
            xSpeed *= reverseSpeed;
            ySpeed *= slowSpeed;
        }

        if(y < 0 || y > 540-30) {
            xSpeed *= slowSpeed;
            ySpeed *= reverseSpeed;
        }*/



        /*if(x < 0)
            x = 0;
        else if (x > 960-30)
            x = 960-30;

        if(y < 0)
            y = 0;
        else if(y > 540-30)
            y = 540-30;*/

        // Accelerate
        xSpeed += (gravity * ax);
        ySpeed += (gravity * ay);

        // Friction
        xSpeed *= friction;
        ySpeed *= friction;
    }

    public void reset() {
        xSpeed = ySpeed = ax = ay = 0;
    }

    private void hitX() {
        xSpeed *= reverseSpeed;
        ySpeed *= slowSpeed;
    }

    private void hitY() {
        xSpeed *= slowSpeed;
        ySpeed *= reverseSpeed;
    }

    private void hitCorner() {

    }

   /* private boolean getLine(ArrayList<Point> points) {

        if(points.size() != 2)
            return false;

        int i = 0;
        if(points.get(0).x > points.get(1).x) {
            i = points.get(1).x;
            points.get(1).x =  points.get(0).x;
            points.get(0).x = i;

            i = points.get(1).y;
            points.get(1).y = points.get(0).y;
            points.get(0).y = i;

            i = 1;
        }

        double v = (points.get(1).y-points.get(0).y)/(points.get(1).x-points.get(0).x);

        double o = Math.atan(v);

        x1 += Math.cos(o)*15;
        points.get(1).y += Math.sin(o)*15;

        double b = y1 - (v*x1);
    }*/

    int intersect2Rect(Point k, Point l, Point m, Point n)
    {
        double det;

        det = (n.x - m.x) * (l.y - k.y)  -  (n.y - m.y) * (l.x - k.x);

        if (det == 0.0)
            return 0 ; // não há intersecção

        intersect.x = (int)(((n.x - m.x) * (m.y - k.y) - (n.y - m.y) * (m.x - k.x))/ det);
        intersect.y = (int)(((l.x - k.x) * (m.y - k.y) - (l.y - k.y) * (m.x - k.x))/ det) ;

        return 1; // há intersecção
    }



    public float getX() {
        return x;
    }

    public float getY() {
        return y;
    }

}
