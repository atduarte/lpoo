package com.lpoo.teeter.views;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.drawable.Drawable;
import android.view.Gravity;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.lpoo.teeter.R;


public class PlayView extends View {

    final int gridDim = 30;

    Drawable background = null;
    Drawable wall = null;
    Drawable ball = null;
    Drawable hole = null;
    Drawable finalHole = null;

    Paint textPaint = null;


    double ballX = 0, ballY = 0;
    public int campo[][];
    boolean rest;


    public PlayView(Context context) {
        super(context);

        // Inicializar imagens
        background = getResources().getDrawable(R.drawable.play_background);
        wall = getResources().getDrawable(R.drawable.wall);
        ball = getResources().getDrawable(R.drawable.ball);
        hole = getResources().getDrawable(R.drawable.hole);
        finalHole = getResources().getDrawable(R.drawable.final_hole);

        textPaint = new Paint();
        textPaint.setColor(Color.WHITE);
        textPaint.setTextSize(35);
        textPaint.setTextAlign(Paint.Align.CENTER);

        campo = new int[18][32];
    }

    @Override
    protected void onDraw(Canvas canvas) {
        // Fundo
        background.setBounds(0,0,getRight(),getBottom());
        background.draw(canvas);

        // Mapa
        for(int i = 0; i < campo.length; i++)
            for(int j = 0; j < campo[i].length; j++) {
                if(campo[i][j] == 1) {
                    wall.setBounds(j*gridDim, i*gridDim, (j+1)*gridDim, (i+1)*gridDim);
                    wall.draw(canvas);
                } else if(campo[i][j] == 2) {
                    hole.setBounds(j*gridDim, i*gridDim, (j+1)*gridDim, (i+1)*gridDim);
                    hole.draw(canvas);
                } else if(campo[i][j] == 4) {
                    finalHole.setBounds(j * gridDim, i * gridDim, (j + 1) * gridDim, (i + 1) * gridDim);
                    finalHole.draw(canvas);
                }
            }

        // Bola
        ball.setBounds((int)ballX,(int)ballY,(int)ballX+30,(int)ballY+30);
        ball.draw(canvas);

        // Pausado
        if(rest) {
            // Overlay
            wall.setBounds(0,0,960,540);
            wall.setAlpha(950);
            wall.draw(canvas);
            wall.setAlpha(1000);

            // Texto
            canvas.drawText("Paused",
                            (canvas.getWidth() / 2),
                            (int)((canvas.getHeight() / 2) - ((textPaint.descent() + textPaint.ascent()) / 2)),
                            textPaint);
        }




    }

    public void setBallX(double BallX) {
        ballX = BallX;
    }

    public void setBallY(double BallY) {
        ballY = BallY;
    }

    public void setRest(boolean Rest) {
        rest = Rest;
    }
}
