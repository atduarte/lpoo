package com.lpoo.teeter.models;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;


public class Reader {

	public Reader()
	{
    }

	public int[][] readerInput(InputStream is) throws IOException
	{
		int matrix[][]=new int[18][32];
		BufferedReader br = new BufferedReader(new InputStreamReader(is, "UTF-8"));
		try {
			String numberList = br.readLine();
			int j=0;

			while (numberList!=null) {

				for(int i=0;i<32;i++)
				{
					matrix[j][i]=Integer.parseInt(numberList.charAt(i)+"");
				}	     
				j++;
                numberList = br.readLine();
			}
		} finally {
			br.close();
		}


		return matrix;



	}

}
